<h1>Feren OS Repositories (Ubuntu 18.04)</h1>

This repository contains the official package repositories for Feren OS machines based on Ubuntu 18.04.

For the actual repositories, check the 'repositories' folder.
